# Curso de redes módulo 1 "Base sólida"
# Abril/Maio 2022

## Público

O foco do curso é para pessoas que atuam ou pretendem atuar nas posições:

- Sysadmin
- Administradores de rede
- Cloud Native Engineer
- Cloud Engineer
- DevOps Engineer
- Plataform Engineer
- GitOps Engineer

## Objetivos

- Começar a construir uma "base sólida" sobre redes para avançar profissionalmente.
- Entender conceitos de rede e seus protocolos.
- Aprimorar a capacidade de administração de serviços de rede.
- Aprimorar os conhecimentos para fazer troubleshooting de rede.

Dinâmica:
- conceitos
- laboratórios
- atividades individuais
- oficinas
- material de apoio

## Conteúdo
- Introdução / objetivos
- Conceitos básicos
- Redes IP
- Máscara de rede
- Cálculo de sub-redes
- DHCP
- Ethernet e ARP
- DNS
- NAT
- DNAT
- Modelo OSI
- Procolos TCP e UDP

## Atividades

- Serão 12 aulas de 2h aproximadamente.
- 6 Laboratórios/tira-dúvidas ≃ 4h cada um.
- carga horária total ≃ 48h

## Material

- As aulas são ao vivo via Jitsi.
- Serão disponibilizados os vídeos para participantes.
- Material de apoio composto de slides, guias e links.

## Datas propostas
Abril

Data| Dia | Horário|     Aula |   Laboratório
:---:|:---:|:---:|:---:|:---:
4   | seg | 20h    | *** |
6   | qua | 20h    | *** |
9   | sáb | 14h    |     | ***
11  | seg | 20h    | *** |
13  | qua | 20h    | *** |
16  | sáb | 14h    |     | ***
18  | seg | 20h    | *** |
20  | qua | 20h    | *** |
23  | sáb | 14h    |     | ***
25  | seg | 20h    | *** |
27  | qua | 20h    | *** |
30  | sáb | 14h    |     | ***

Maio

Data| Dia | Horário|     Aula |   Laboratório
:---:|:---:|:---:|:---:|:---:
 2  | seg | 20h    | *** |
 4  | qua | 20h    | *** |
 7  | sáb | 14h    |     | ***
 9  | seg | 20h    | *** |
11  | qua | 20h    | *** |
14  | sáb | 14h    |     | ***


-------
## calendário
Abril

Seg  | Ter | Qua | Qui | Sex | Sáb
:---:|:---:|:---:|:---:|:---:|:---:|
 [4] |  .  |  [6] | .  |  .  |  [[9]]
[11] |  .  | [13] | .  |  .  | [[16]]
[18] |  .  | [20] | .  |  .  | [[23]]
[25] |  .  | [27] | .  |  .  | [[30]]

Maio

Seg  | Ter | Qua | Qui | Sex | Sáb
:---:|:---:|:---:|:---:|:---:|:---:|
 [2] |  .  |  [4] | .  |  .  |  [[7]]
 [9] |  .  | [11] | .  |  .  | [[14]]
-------

```
[aula]
[[Laboratório]]
```

## Custo e condições

- R$ 320,00 via pix ou parcelado no cartão de crédito via pagseguro.

**(Meninas tem 30% de desconto)**

Obs.: O número de parcelas você pode escolher junto a sua operadora de cartão de crédito.

Pix:
kretcheu@gmail.com

PagSeguro
https://pag.ae/7Y67ngrSb

com 30% de desconto para mulheres:
https://pag.ae/7Y73evy65

## Contato
Qualquer dúvida entre em contato:

- e-mail: kretcheu@gmail.com
- Telegram: @kretcheu

