## Exercícios sub-redes

1. Qual é o endereço de broadcast da seguinte subrede 192.168.229.104/29?

2. Qual é o primeiro host válido da subrede que o seguinte host pertence 192.168.145.221/27?

3. Qual é o endereço de broadcast da seguinte subrede 172.18.200.0 255.255.252.0?

4. Qual o último host válido da seguinte subrede 192.168.137.152 255.255.255.248?

5. Qual é o primeiro host válido da subrede que o seguinte host pertence 172.18.40.122 255.255.254.0?

6. Qual o último host válido da seguinte subrede 172.27.68.0 255.255.255.128?

7. Qual o último host válido da seguinte subrede 172.27.251.128/25?

8. Qual é o endereço de broadcast da seguinte subrede 172.18.188.0 255.255.254.0?

9. Qual o último host válido da seguinte subrede 172.28.16.0/20?

10. Qual o último host válido da seguinte subrede 172.20.190.128/25?

11. Qual o último host válido da seguinte subrede 172.23.149.128 255.255.255.224?

12. Qual é o primeiro host válido da subrede que o seguinte host pertence 192.168.17.29 255.255.255.252?

13. Qual é o primeiro host válido da subrede que o seguinte host pertence 172.30.110.108/20?

14. Quantas subredes e hosts por subrede pode-se obter usando a seguinte rede 192.168.180.0/28?

15. Qual a faixa válida para hosts que o endereço IP 172.17.38.221/21 faz parte?


