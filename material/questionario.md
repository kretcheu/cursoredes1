## Questionário

1. Defina rede.
2. O que um número binário?
3. Qual o tamanho do endereço IP?
4. Quais as classes de IP e suas máscaras padrão?
5. Quais os intervalos de IPs reservados para cada classe?
6. O que a máscara de rede define?
7. O que é um gateway?
8. O que uma rota determina?
9. O que é uma sub-rede?
10. O que é CIDR e para que serve?
11. Quantos hosts podem ter numa rede /26, /17?
12. Descreva o MAC Address.
13. Para que server o protocolo DHCP?
14. Quais as etapas da comunicação DHCP?
15. O que é e para que server o MAC FF:FF:FF:FF:FF:FF?
16. O que é BIND na configuração do DHCP?
17. Para que serve o protocolo ARP?
18. Quais as etapas do ARP?
19. O que é ARP-Spoofing?
20. Para que serve DNS?
21. Quantos e quais os servidores DNS-ROOT?
22. Quais os IPs dos DNS responsáveis pelo .br?
23. Qual o servidor responsável pelo domínio fsfla.org
24. Qual servidor responsável por receber e-mails enviados para gnu.org
25. Descreva as etapas de estabelecimento e encerramento de uma conexão TCP.
26. Compare TCP e UDP, vantagens e desvantagens de cada um.
27. O que é porta e quantos bits tem o número de uma porta?
28. O que faz NAT?
29. Quais informações são alteradas quando se faz NAT?
30. Onde e porque se usa DNAT?
