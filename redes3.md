# Curso de redes módulo 1 "Base sólida"

## Público

O foco do curso é para pessoas que atuam ou pretendem atuar nas posições:

- Sysadmin
- Administradores de rede
- Cloud Native Engineer
- Cloud Engineer
- DevOps Engineer
- Plataform Engineer
- GitOps Engineer

## Objetivos

- Começar a construir uma "base sólida" sobre redes para avançar profissionalmente.
- Entender conceitos de rede e seus protocolos.
- Aprimorar a capacidade de administração de serviços de rede.
- Aprimorar os conhecimentos para fazer troubleshooting de rede.

Dinâmica:
- conceitos
- laboratórios
- atividades individuais
- oficinas
- material de apoio

## Conteúdo
- Introdução / objetivos
- Conceitos básicos
- Redes IP
- Máscara de rede
- Cálculo de sub-redes
- DHCP
- Ethernet e ARP
- DNS
- NAT
- DNAT
- Modelo OSI
- Procolos TCP e UDP

## Atividades

- São 12 aulas de 2h aproximadamente.
- 6 Laboratórios/tira-dúvidas.
- carga horária total ≃ 48h

## Material

- Serão disponibilizados os vídeos para participantes.
- Material de apoio composto de slides, guias e links.

## Custo e condições

- R$ 256,00 via pix ou parcelado no cartão de crédito via pagseguro.

Obs.: O número de parcelas você pode escolher junto a sua operadora de cartão de crédito.

Pix:
kretcheu@gmail.com

PagSeguro
https://pag.ae/7Yk9GAbb1

## Contato
Qualquer dúvida entre em contato:

- e-mail: kretcheu@gmail.com
- Telegram: @kretcheu

