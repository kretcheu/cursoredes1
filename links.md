## Links de estudos

- IP Protocol
https://datatracker.ietf.org/doc/html/rfc791

- IPv6
https://datatracker.ietf.org/doc/html/rfc2460

- IP Address
https://en.wikipedia.org/wiki/IP_address

- MacAddress
https://pt.wikipedia.org/wiki/Endere%C3%A7o_MAC

- DHCP
https://en.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol

- Wireshark Learn
https://www.wireshark.org/?hl=pt-BR#learnWS

- ARP
https://pt.wikipedia.org/wiki/Address_Resolution_Protocol

- DNS
https://en.wikipedia.org/wiki/Root_name_server

- TCP
https://www.youtube.com/watch?v=gK3gl3Vh8L0

- Modelo OSI
https://pt.wikipedia.org/wiki/Modelo_OSI

