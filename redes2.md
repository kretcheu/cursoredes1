# Curso de redes módulo 1 "Base sólida"
# Maio/Junho 2022

## Público

O foco do curso é para pessoas que atuam ou pretendem atuar nas posições:

- Sysadmin
- Administradores de rede
- Cloud Native Engineer
- Cloud Engineer
- DevOps Engineer
- Plataform Engineer
- GitOps Engineer

## Objetivos

- Começar a construir uma "base sólida" sobre redes para avançar profissionalmente.
- Entender conceitos de rede e seus protocolos.
- Aprimorar a capacidade de administração de serviços de rede.
- Aprimorar os conhecimentos para fazer troubleshooting de rede.

Dinâmica:
- conceitos
- laboratórios
- atividades individuais
- oficinas
- material de apoio

## Conteúdo
- Introdução / objetivos
- Conceitos básicos
- Redes IP
- Máscara de rede
- Cálculo de sub-redes
- DHCP
- Ethernet e ARP
- DNS
- NAT
- DNAT
- Modelo OSI
- Procolos TCP e UDP

## Atividades

- Serão 12 aulas de 2h aproximadamente.
- 6 Laboratórios/tira-dúvidas ≃ 4h cada um.
- carga horária total ≃ 48h

## Material

- As aulas são ao vivo via Jitsi.
- Serão disponibilizados os vídeos para participantes.
- Material de apoio composto de slides, guias e links.

## Datas propostas
Maio

Data| Dia | Horário|     Aula |   Laboratório
:---:|:---:|:---:|:---:|:---:
16  | seg | 20h    | *** |
18  | qua | 20h    | *** |
21  | sáb | 14h    |     | ***
23  | seg | 20h    | *** |
25  | qua | 20h    | *** |
28  | sáb | 14h    |     | ***
30  | seg | 20h    | *** |

Junho

Data| Dia | Horário|     Aula |   Laboratório
:---:|:---:|:---:|:---:|:---:
1   | qua | 20h    | *** |
4   | sáb | 14h    |     | ***
6   | seg | 20h    | *** |
8   | qua | 20h    | *** |
11  | sáb | 14h    |     | ***
13  | seg | 20h    | *** |
15  | qua | 20h    | *** |
18  | sáb | 14h    |     | ***
20  | seg | 20h    | *** |
22  | qua | 20h    | *** |
25  | sáb | 14h    |     | ***

-------
## calendário
Maio

Seg  | Ter | Qua | Qui | Sex | Sáb
:---:|:---:|:---:|:---:|:---:|:---:|
[16] |  .  | [18] | .  |  .  | [[21]]
[23] |  .  | [25] | .  |  .  | [[28]]
[30] |  .  |  .   |    |  .  |  .

Junho

Seg  | Ter | Qua | Qui | Sex | Sáb
:---:|:---:|:---:|:---:|:---:|:---:|
  .  |  .  |  [1] | .  |  .  |  [[4]]
 [6] |  .  |  [8] | .  |  .  | [[11]]
[13] |  .  | [15] | .  |  .  | [[18]]
[20] |  .  | [22] | .  |  .  | [[25]]
-------

```
[ala]
[[Laboratório]]
```

## Custo e condições

- R$ 320,00 via pix ou parcelado no cartão de crédito via pagseguro.

- ** Inscrições feitas em abril tem 10% de desconto.
- ** Mulheres cis e trans tem 30% de desconto.
- ** Alunos de outros cursos tem 20% de desconto.
- ** Se conseguir um aluno ambos terão 20% de desconto.

Obs.: O número de parcelas você pode escolher junto a sua operadora de cartão de crédito.

Pix:
kretcheu@gmail.com

PagSeguro
https://pag.ae/7Y67ngrSb

com 30% de desconto para mulheres:
https://pag.ae/7Y73evy65

com 20% de desconto para duplas:
https://pag.ae/7Y9fMv3zs

com 10% de desconto para inscrições em abril.
https://pag.ae/7Y9qhNCuo

## Contato
Qualquer dúvida entre em contato:

- e-mail: kretcheu@gmail.com
- Telegram: @kretcheu

